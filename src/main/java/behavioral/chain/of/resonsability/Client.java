package behavioral.chain.of.resonsability;

import java.time.LocalDate;

public class Client {
    public static void main(String[] args) {
        LeaveApplication application = LeaveApplication.getBuilder()
                .withType(LeaveApplication.Type.Sick)
                .from(LocalDate.now())
                .to(LocalDate.of(2020, 06, 30))
                .build();

        System.out.println(application);

        LeaveApprover approver = createChain();
        approver.processLeaveApplication(application);

        System.out.println(application);
    }

    public static LeaveApprover createChain() {
        Director director = new Director(null);
        Manager manager = new Manager(director);
        ProjectLead lead = new ProjectLead(manager);

        return lead;
    }
}
