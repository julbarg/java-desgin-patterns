package behavioral.chain.of.resonsability;

/**
 * Abstract hanlder
 */
public abstract class Employee implements LeaveApprover {

    private String role;

    private LeaveApprover succesor;
    public Employee(String role, LeaveApprover succesor) {
        this.role = role;
        this.succesor = succesor;
    }

    @Override
    public void processLeaveApplication(LeaveApplication application) {
        if(!processRequest(application) && succesor != null) {
            succesor.processLeaveApplication(application);
        }
    }

    protected abstract boolean processRequest (LeaveApplication application);

    @Override
    public String getApproverRole() {
        return role;
    }
}
