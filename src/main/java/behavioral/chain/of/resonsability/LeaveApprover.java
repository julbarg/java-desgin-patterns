package behavioral.chain.of.resonsability;

/**
 * This represents a handler in chain responsability
 */
public interface LeaveApprover {

    void processLeaveApplication(LeaveApplication application);

    String getApproverRole();
}
