package behavioral.chain.of.resonsability;

/**
 * A concrete hanlder
 */
public class ProjectLead extends Employee {

    public ProjectLead(LeaveApprover succesor) {
        super("Project Lead", succesor);
    }

    @Override
    protected boolean processRequest(LeaveApplication application) {
        // type is sick leave & duration is less than or equal to  days
        if (application.getType() == LeaveApplication.Type.Sick) {
            if (application.getNoOfDays() <= 2) {
                application.approve(getApproverRole());

                return true;
            }
        }
        return false;
    }
}
