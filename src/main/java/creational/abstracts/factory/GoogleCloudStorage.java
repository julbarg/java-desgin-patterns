package creational.abstracts.factory;

public class GoogleCloudStorage implements Storage {

    public GoogleCloudStorage (int capacityInMbi) {
        // Use GCP API
        System.out.println("Allocated " + capacityInMbi + " on Google Cloud Storage");
    }
    @Override
    public String getId() {
        return "gcpcs1";
    }

    @Override
    public String toString() {
        return "Google Cloud Storage";
    }
}
