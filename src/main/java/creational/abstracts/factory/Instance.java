package creational.abstracts.factory;

/**
 * Represents an abstract product
 */
public interface Instance {
    void start();
    enum Capacity {micro, small, large}


    void attachStorage(Storage storage);

    void stop();
}
