package creational.abstracts.factory;

public interface Storage {

    String getId();
}
