package creational.builder;

import java.time.LocalDate;

public class Client {

    public static void main(String[] args) {
        User user = createUser();
        UserDTOBuilder builder = new UserWebDTOBuilder();

        UserDTO userDTO = directBuild(builder, user);

        System.out.println(userDTO.toString());

    }

    // Director
    private static UserDTO directBuild(UserDTOBuilder builder, User user) {
        return builder
                .wihBirthday(user.getBirthday())
                .withAddress(user.getAddress())
                .withFirstName(user.getFirstName())
                .withLastName(user.getLastName())
                .build();
    }

    public static User createUser() {
        User user = new User();
        user.setBirthday(LocalDate.of(1960, 5, 6));
        user.setFirstName("Julian");
        user.setLastName("Barragan");

        Address address = new Address();
        address.setHouseNumber("401");
        address.setStreet("Carrera 28");
        address.setCity("Bogota");
        address.setState("Indiana");
        address.setZipcode("47998");

        user.setAddress(address);

        return user;
    }
}
