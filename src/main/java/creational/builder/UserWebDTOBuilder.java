package creational.builder;

import java.time.LocalDate;
import java.time.Period;

/**
 * The concrete builder for UserWebDTO
 */
public class UserWebDTOBuilder implements  UserDTOBuilder{

    private String firstName;
    private String lastName;
    private String age;
    private String address;

    private UserDTO userDTO;


    @Override
    public UserDTOBuilder withFirstName(String name) {
        firstName = name;
        return this;
    }

    @Override
    public UserDTOBuilder withLastName(String lname) {
        lastName = lname;
        return this;
    }

    @Override
    public UserDTOBuilder withAddress(Address address) {
        this.address = address.getHouseNumber() + ", " + address.getState() + ", " + address.getCity() + ", " + address.getState() + ", " + address.getZipcode();
        return this;
    }

    @Override
    public UserDTOBuilder wihBirthday(LocalDate date) {
        Period ageInYears = Period.between(date, LocalDate.now());
        age = Integer.toString(ageInYears.getYears());
        return this;
    }

    @Override
    public UserDTO build() {
        userDTO = new UserWebDTO(firstName + " " +lastName, address, age);
        return userDTO;
    }

    @Override
    public UserDTO getUserDTO() {
        return userDTO;
    }
}
