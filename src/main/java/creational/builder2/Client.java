package creational.builder2;

import creational.builder.Address;
import creational.builder.User;

import java.time.LocalDate;

public class Client {

    public static void main(String[] args) {
        User user = createUser();

        UserDTO userDTO = directBuild(UserDTO.getBuilder(), user);

        System.out.println(userDTO);
    }

    private static UserDTO directBuild(UserDTO.UserDTOBuilder builder, User user) {
        return builder.withAddress(user.getAddress())
                .withBirthday(user.getBirthday())
                .withFirstName(user.getFirstName())
                .withLastName(user.getLastName())
                .build();
    }

    public static User createUser() {
        User user = new User();
        user.setBirthday(LocalDate.of(1960, 5, 6));
        user.setFirstName("Julian");
        user.setLastName("Barragan");

        Address address = new Address();
        address.setHouseNumber("401");
        address.setStreet("Carrera 28");
        address.setCity("Bogota");
        address.setState("Indiana");
        address.setZipcode("47998");

        user.setAddress(address);

        return user;
    }
}
