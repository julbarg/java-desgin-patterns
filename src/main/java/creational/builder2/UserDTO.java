package creational.builder2;

import creational.builder.Address;

import java.time.LocalDate;
import java.time.Period;

/**
 * Immutable class
 */
public class UserDTO {

    private String name;

    private String address;

    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return age;
    }

    private void setAge(String age) {
        this.age = age;
    }

    public static UserDTOBuilder getBuilder() {
        return new UserDTOBuilder();
    }

    @Override
    public String toString() {
        return "name=" + name + "\nage=" + age + "\naddress=" + address ;
    }

    // Builder
    public static class UserDTOBuilder {
        private String firstName;

        private String lastName;

        private String age;

        private String address;

        private UserDTO userDTO;

        public UserDTOBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserDTOBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserDTOBuilder withBirthday(LocalDate date) {
            this.age = Integer.toString(Period.between(date, LocalDate.now()).getYears());
            return this;
        }

        public UserDTOBuilder withAddress(Address address) {
            this.address = address.getHouseNumber() + " " + address.getStreet() + " " + address.getCity() + " " + address.getState() + " " + address.getZipcode();
            return this;
        }

        public UserDTO build() {
            this.userDTO = new UserDTO();
            userDTO.setAddress(this.address);
            userDTO.setAge(this.age);
            userDTO.setName(this.firstName + " " + this.lastName);

            return this.userDTO;
        }

        public UserDTO getUserDTO() {
            return this.userDTO;
        }
    }
}
