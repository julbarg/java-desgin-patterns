package creational.factory.method;

import creational.factory.method.message.JSONMessage;
import creational.factory.method.message.Message;

/**
 * Provides implementation for creating JSON messages
 */
public class JSONMessageCreator extends MessageCreator {

    @Override
    public Message createMessage() {
        return new JSONMessage();
    }
}
