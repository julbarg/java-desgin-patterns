package creational.factory.method;

import creational.factory.method.message.Message;

/**
 * This is our abstract creator
 * The abstract method createMessage() has to be implement by its subclasses
 */
public abstract class MessageCreator {

    /**
     * This is called by clients
     * @return
     */
    public Message getMessage() {
        Message msg = createMessage();

        msg.addDefaultHeaders();
        msg.encrypt();

        return msg;
    }

    /**
     * Subclasses must provide implementation for this & return
     * a specific Message subclass
     * @return
     */
    // Factory method
    protected abstract Message createMessage();
}
