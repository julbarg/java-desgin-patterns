package head.first.design.patterns.strategy.adventure.game;

public class AxeBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Using a Axe");
    }
}
