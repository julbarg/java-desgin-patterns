package head.first.design.patterns.strategy.adventure.game;

public class BowAndArrowBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Using Bow and Arrow");
    }
}
