package head.first.design.patterns.strategy.adventure.game;

public abstract class Character {

    private WeaponBehavior weapon;

    public abstract void fight();


    public Character() {
    }

    public Character(WeaponBehavior weapon) {
        this.weapon = weapon;
    }

    public void performUseWeapon() {
        weapon.useWeapon();
    }

    public void setWeapon(WeaponBehavior weapon) {
        this.weapon = weapon;
    }
}
