package head.first.design.patterns.strategy.adventure.game;

public class King extends Character {

    @Override
    public void fight() {
        System.out.println("King fight");
    }
}
