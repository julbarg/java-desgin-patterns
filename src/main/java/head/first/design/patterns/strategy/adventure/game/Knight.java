package head.first.design.patterns.strategy.adventure.game;

public class Knight extends Character {

    @Override
    public void fight() {
        System.out.println("Knight fight");
    }
}
