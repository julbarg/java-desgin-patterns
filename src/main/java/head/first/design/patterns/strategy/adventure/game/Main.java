package head.first.design.patterns.strategy.adventure.game;

public class Main {

    public static void main(String[] args) {
        King king = new King();
        king.setWeapon(new BowAndArrowBehavior());
        king.performUseWeapon();

        Queen queen = new Queen(new KnifeBehavior());
        queen.performUseWeapon();
    }
}
