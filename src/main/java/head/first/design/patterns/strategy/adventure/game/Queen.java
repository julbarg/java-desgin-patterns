package head.first.design.patterns.strategy.adventure.game;

public class Queen extends Character {

    public Queen(WeaponBehavior weapon) {
        super(weapon);
    }

    @Override
    public void fight() {
        System.out.println("Queen fight!!!");
    }
}
