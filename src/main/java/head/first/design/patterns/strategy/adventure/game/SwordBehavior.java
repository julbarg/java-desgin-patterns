package head.first.design.patterns.strategy.adventure.game;

public class SwordBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Use a Sword");
    }
}
