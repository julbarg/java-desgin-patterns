package head.first.design.patterns.strategy.adventure.game;

public class Troll extends Character {

    @Override
    public void fight() {
        System.out.println("Troll fight");
    }
}
