package head.first.design.patterns.strategy.adventure.game;

public interface WeaponBehavior {

    void useWeapon();
}
