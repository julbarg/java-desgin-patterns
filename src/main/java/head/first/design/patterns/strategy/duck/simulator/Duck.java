package head.first.design.patterns.strategy.duck.simulator;

public abstract class Duck {

    /**
     * Declare two references variables for the behavior interface types.
     * All duck subclasses (in the same package) inherit these
     */
    FlyBehavior flyBehavior;

    QuackBehavior quackBehavior;

    public Duck() {

    }

    public Duck(FlyBehavior flyBehavior, QuackBehavior quackBehavior) {
        this.flyBehavior = flyBehavior;
        this.quackBehavior = quackBehavior;
    }

    public abstract void display();

    public void performFly() {
        // Delegate to the behavior class
        flyBehavior.fly();
    }

    public void performQuack() {
        // Delegate to the behavior class
        quackBehavior.quack();
    }

    public void swim() {
        System.out.println("All ducks float, evend decoys!");
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

}
