package head.first.design.patterns.strategy.duck.simulator;

public interface FlyBehavior {
    void fly();
}
