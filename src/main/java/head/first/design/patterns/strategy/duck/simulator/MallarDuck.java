package head.first.design.patterns.strategy.duck.simulator;

/**
 * Remember, MallardDuck inherits the quackBehavior and flyBehavior instance
 * variables form class Duck.
 */
public class MallarDuck extends Duck {

    public MallarDuck() {
        /**
         * A MallardDuck uses the Quack class to handle its quack, so when performQuack
         * is called, the responsability for the quack is delegated to the Quack object
         * and we get a real quack.
         */
        quackBehavior = new Quack();
        /**
         * And it uses FlyWithWings as its FlyBehavior type
         */
        flyBehavior = new FlyWithWings();
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
}
