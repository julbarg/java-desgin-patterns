package head.first.design.patterns.strategy.duck.simulator;

public class ModelQuack extends Duck {

    public ModelQuack() {
        // Our model ducks begins life grounded... without a way to fly
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}
