package head.first.design.patterns.strategy.duck.simulator;

public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Quack");
    }
}
