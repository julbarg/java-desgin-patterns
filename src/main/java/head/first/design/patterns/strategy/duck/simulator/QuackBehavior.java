package head.first.design.patterns.strategy.duck.simulator;

public interface QuackBehavior {
    void quack();
}
