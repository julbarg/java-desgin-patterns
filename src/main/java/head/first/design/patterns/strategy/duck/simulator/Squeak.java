package head.first.design.patterns.strategy.duck.simulator;

public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Squeak");
    }
}
