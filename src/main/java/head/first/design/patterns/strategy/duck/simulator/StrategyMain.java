package head.first.design.patterns.strategy.duck.simulator;

public class StrategyMain {

    public static void main(String[] args) {
        Duck mallard = new MallarDuck();
        /**
         * This calls the MallardDuck's inherited performQuack() method, which delegates
         * to the object's QuackBehavior (i.e. calls quack() on the duck's inherited
         * quackBehavior reference)
         */
        mallard.performQuack();
        /**
         * Then we do the same thing with MallardDuck's inherited performFly() method.
         */
        mallard.performFly();
        mallard.swim();

        System.out.println("\nDuck Model");

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
