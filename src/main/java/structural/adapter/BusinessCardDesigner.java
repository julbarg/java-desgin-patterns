package structural.adapter;

/**
 * Main code which requires Customer interface
 */
public class BusinessCardDesigner {

    public String designCard(Customer customer) {
        return new StringBuilder().append(customer.getName())
                .append("\n" + customer.getDesignation())
                .append("\n" + customer.getAddress())
                .toString();
    }
}
