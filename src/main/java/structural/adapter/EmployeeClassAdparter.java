package structural.adapter;

/**
 * A class adapter, works as Two-way adapter
 */
public class EmployeeClassAdparter extends Employee implements Customer {


    @Override
    public String getName() {
        return this.getFullName();
    }

    @Override
    public String getDesignation() {
        return this.getJobTittle();
    }

    @Override
    public String getAddress() {
        return getOfficeLocation();
    }
}
