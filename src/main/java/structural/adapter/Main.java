package structural.adapter;

public class Main {

    public static void main(String[] args) {
        /** Using Class/Two-way adapter **/
        EmployeeClassAdparter adapter = new EmployeeClassAdparter();
        populateEmployeeData(adapter);

        BusinessCardDesigner designer = new BusinessCardDesigner();

        Customer target = new EmployeeObjectAdapter(new Employee());
        designer.designCard(target);

        String card = designer.designCard(adapter);
        System.out.println(card);


        System.out.println();

        /** Using Object Adapter **/
        Employee employee = new Employee();
        populateEmployeeData(employee);
        EmployeeObjectAdapter objectAdapter = new EmployeeObjectAdapter(employee);

        card = designer.designCard(objectAdapter);
        System.out.println(card);


    }

    private static void populateEmployeeData (Employee employee) {
        employee.setFullName("Julian Barragan");
        employee.setJobTittle("Software Developer");
        employee.setOfficeLocation("Bogota, Colombia");
    }
}
